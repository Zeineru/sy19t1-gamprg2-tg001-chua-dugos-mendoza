// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthPoints.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOTA2CLONERETRY_API UHealthPoints : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthPoints();

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		int32 baseHP = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		float maxHP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
		float currentHP;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	void SetInitialHealth(float strength);

	float Heal(float value);

	float Damage(float value);

	void SetHealth(float value);

	void HealthRegeneration(float baseValue, float strength);

	float NormalizeHP();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
