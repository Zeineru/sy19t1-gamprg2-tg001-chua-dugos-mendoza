// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthPoints.h"

// Sets default values for this component's properties
UHealthPoints::UHealthPoints()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthPoints::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UHealthPoints::SetInitialHealth(float strength){
	maxHP = baseHP + (strength * 12);
}

float UHealthPoints::Heal(float value){
	value = FMath::Min(this->maxHP - this->currentHP, value);
	this->currentHP += value;
	return value;
}

float UHealthPoints::Damage(float value){
	value = FMath::Min(this->currentHP, value);
	this->currentHP -= value;
	return value;
}

void UHealthPoints::SetHealth(float value){
	this->maxHP = value;
}

void UHealthPoints::HealthRegeneration(float baseValue, float strength){
	float regenValue = baseValue * (strength * 0.1f);
	//this is wierd...
}

float UHealthPoints::NormalizeHP()
{
	return this->currentHP / this->maxHP;
}


// Called every frame
void UHealthPoints::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

