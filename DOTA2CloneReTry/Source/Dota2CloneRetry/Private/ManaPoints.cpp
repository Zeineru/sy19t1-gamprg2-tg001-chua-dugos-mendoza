// Fill out your copyright notice in the Description page of Project Settings.

#include "ManaPoints.h"

// Sets default values for this component's properties
UManaPoints::UManaPoints()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UManaPoints::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UManaPoints::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

