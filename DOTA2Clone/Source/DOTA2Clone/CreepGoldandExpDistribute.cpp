// Fill out your copyright notice in the Description page of Project Settings.

#include "CreepGoldandExpDistribute.h"

// Sets default values for this component's properties
UCreepGoldandExpDistribute::UCreepGoldandExpDistribute()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	minGoldMelee = 32;
	maxGoldMelee = 38;
	minGoldRanged = 48;
	maxGoldRanged = 58;
	ExpRelease = 69; //nice
	// ...
}


// Called when the game starts
void UCreepGoldandExpDistribute::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UCreepGoldandExpDistribute::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UCreepGoldandExpDistribute::ReleaseGold()
{

}

void UCreepGoldandExpDistribute::ReleaseGoldRanged(UActorGold* wallet)
{
	int32 randomGold = rand() % minGoldRanged + maxGoldRanged;
	wallet->setGold(randomGold);
}

void UCreepGoldandExpDistribute::ReleaseGoldMelee(UActorGold* wallet)
{
	int32 randomGold = rand() % minGoldMelee + maxGoldMelee;
	wallet->setGold(randomGold);
}

