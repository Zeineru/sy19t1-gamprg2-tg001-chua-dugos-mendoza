// Fill out your copyright notice in the Description page of Project Settings.

#include "GoldManager.h"

// Sets default values
AGoldManager::AGoldManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AGoldManager::BeginPlay()
{
	Super::BeginPlay();
	
}

int32 AGoldManager::gainGold(int value, UActorGold* wallet)
{
	wallet->setGold(value);
	return value;
}

int32 AGoldManager::spendGold(int value, UActorGold* wallet)
{
	wallet->setGold(value * -1);
	return value;
}

int AGoldManager::releaseInitialGold(int value, UActorGold* wallet)
{
	wallet->setGold(value);
	return value;
}

// Called every frame
void AGoldManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

