// Fill out your copyright notice in the Description page of Project Settings.

#include "InventoryManager.h"

// Sets default values
AInventoryManager::AInventoryManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AInventoryManager::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInventoryManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

int AInventoryManager::GiveItem(AItemObject * item)
{
	return 0;
}

int AInventoryManager::SellItem(AItemObject * item)
{
	return 0;
}

