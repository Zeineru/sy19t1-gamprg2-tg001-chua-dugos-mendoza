// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SkillPointSystem.h"
#include "ExpirenceSystem.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOTA2CLONE_API UExpirenceSystem : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UExpirenceSystem();



protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExpirenceSystem")
		int32 currentExpirence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExpirenceSystem")
		int32 nextExpirence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExpirenceSystem")
		int32 currentLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExpirenceSystem")
		TArray<int32> exprienceList;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExpirenceSystem")
		int32 baseExp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExpirenceSystem")
		int32 expMultiplyer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExpirenceSystem")
		TArray<int32> KillingSpreeBounty;

	// Called when the game starts
	virtual void BeginPlay() override;



public:	
	// Called every frame
	UFUNCTION(BlueprintCallable, Category = "Experience")
	int GainExp(int value);
	UFUNCTION(BlueprintCallable, Category = "Experience")
	void LevelUp();
	UFUNCTION(BlueprintCallable, Category = "Experience")
	int CalculateLeftExp();
	UFUNCTION(BlueprintCallable, Category = "Experience")
	int GetCurLevel();
	UFUNCTION(BlueprintCallable, Category = "Experience")
	int ExpRelease(int KillingSpree);

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
