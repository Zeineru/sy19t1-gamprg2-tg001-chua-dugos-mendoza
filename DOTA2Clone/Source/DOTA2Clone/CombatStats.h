// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CombatStats.generated.h"


USTRUCT(BlueprintType)
struct DOTA2CLONE_API FBaseModValue {
	GENERATED_BODY()

public:
	FBaseModValue();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "modValue")
	float baseValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "modValue")
	float factor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "modValue")
	float additive;

	float CalculateValue();

};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOTA2CLONE_API UCombatStats : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCombatStats();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CombatStats")
		FBaseModValue Armor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CombatStats")
		FBaseModValue MagicRsst;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CombatStats")
		FBaseModValue SpellDmg;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CombatStats")
		FBaseModValue Evade;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CombatStats")
		FBaseModValue MoveSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CombatStats")
		FBaseModValue TurnRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CombatStats")
		FBaseModValue AtkRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CombatStats")
		FBaseModValue CastRange;

	UFUNCTION(BlueprintCallable, Category = "CombatStats")
		float GetCalculatedValue(FBaseModValue value);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
