// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorGold.h"

// Sets default values for this component's properties
UActorGold::UActorGold()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UActorGold::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


int UActorGold::setGold(int value)
{
	this->userGold += value;
	return value;
}

// Called every frame
void UActorGold::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

