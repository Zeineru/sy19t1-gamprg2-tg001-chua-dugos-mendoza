// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseAttributes.h"
#include "Hero.generated.h"

class UBaseAttributes;

UCLASS()
class DOTA2CLONE_API AHero : public AActor
{
	GENERATED_BODY()
	
	public:	
	// Sets default values for this actor's properties
	AHero();

	protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite)
		class UBaseAttributes* attributes;

	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};