// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DOTA2CloneGameMode.generated.h"

UCLASS(minimalapi)
class ADOTA2CloneGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ADOTA2CloneGameMode();
};



