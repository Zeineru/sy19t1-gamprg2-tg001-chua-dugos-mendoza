// Fill out your copyright notice in the Description page of Project Settings.

#include "WitchDoctor.h"

// Sets default values for this component's properties
UWitchDoctor::UWitchDoctor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWitchDoctor::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UWitchDoctor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

