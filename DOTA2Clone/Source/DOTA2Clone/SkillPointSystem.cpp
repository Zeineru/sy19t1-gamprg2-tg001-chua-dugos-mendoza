// Fill out your copyright notice in the Description page of Project Settings.

#include "SkillPointSystem.h"

// Sets default values for this component's properties
USkillPointSystem::USkillPointSystem()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USkillPointSystem::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


void USkillPointSystem::GainSkillPoint()
{
	unusedSkillPoint++;
	totalSkillPoint++;
}

void USkillPointSystem::spendSkillPoint()
{
	unusedSkillPoint--;
}

// Called every frame
void USkillPointSystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

