// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BaseAttributes.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOTA2CLONE_API UBaseAttributes : public UActorComponent
{
	GENERATED_BODY()

	public:	
	// Sets default values for this component's properties
	UBaseAttributes();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributes")
	int32 str;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributes")
	int32 agi;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributes")
	int32 intl;
	

	public:	

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	int GetStrength();

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	int GetAgility();

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	int GetIntelligence();

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	void SetStrength(int strValue);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	void SetAgility(int agiValue);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	void SetIntelligence(int intlValue);
};