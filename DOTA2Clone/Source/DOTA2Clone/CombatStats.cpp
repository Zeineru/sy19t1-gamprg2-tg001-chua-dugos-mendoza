// Fill out your copyright notice in the Description page of Project Settings.

#include "CombatStats.h"

// Sets default values for this component's properties
UCombatStats::UCombatStats()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


float UCombatStats::GetCalculatedValue(FBaseModValue value)
{
	return value.CalculateValue();
}

// Called when the game starts
void UCombatStats::BeginPlay()
{

	// ...
	
}


// Called every frame
void UCombatStats::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{

	// ...
}
FBaseModValue::FBaseModValue() {

}

float FBaseModValue::CalculateValue()
{
	return (this->baseValue + this->additive) * this->factor;
}
