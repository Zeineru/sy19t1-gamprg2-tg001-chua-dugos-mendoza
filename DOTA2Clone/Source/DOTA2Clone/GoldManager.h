// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActorGold.h"
#include "GoldManager.generated.h"


class UActorGold;

UCLASS()
class DOTA2CLONE_API AGoldManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGoldManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	UFUNCTION(BlueprintCallable, Category = "Gold")
		int gainGold(int value, UActorGold* wallet);

	UFUNCTION(BlueprintCallable, Category = "Gold")
		int spendGold(int value, UActorGold* wallet);

	UFUNCTION(BlueprintCallable, Category = "Gold")
		int releaseInitialGold(int value, UActorGold* wallet);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
