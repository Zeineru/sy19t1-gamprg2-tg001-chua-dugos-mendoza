// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ManaPoints.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOTA2CLONE_API UManaPoints : public UActorComponent
{
	GENERATED_BODY()

	public:	
	// Sets default values for this component's properties
	UManaPoints();

	protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	int32 baseMP = 75;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	float maxMP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	float currentMP;

	// Called when the game starts
	virtual void BeginPlay() override;

	public:

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	void SetInitialMana(float intelligence);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float Restore(float value);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float Deplete(float value);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	void SetMana(float value);

	public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
};