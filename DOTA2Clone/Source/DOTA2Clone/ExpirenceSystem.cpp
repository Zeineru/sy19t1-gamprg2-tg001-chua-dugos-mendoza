// Fill out your copyright notice in the Description page of Project Settings.

#include "ExpirenceSystem.h"

// Sets default values for this component's properties
UExpirenceSystem::UExpirenceSystem()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	//int32 expListTemp[] = { 0,230,600,1080,1660,2260,2980,3730,4510,5320,6160,7030,7930,9155,10405,11680,12980,14305,15805,17395,18995,20845,22945,25295,27895 };
	//exprienceList.Append(expListTemp,ARRAY_COUNT(expListTemp));
	exprienceList = { 0,230,600,1080,1660,2260,2980,3730,4510,5320,6160,7030,7930,9155,10405,11680,12980,14305,15805,17395,18995,20845,22945,25295,27895 };
	KillingSpreeBounty = {0,0,0,400,600,800,1000,1200,1400,1600,1800};
	baseExp = 40;
	// ...
}


// Called when the game starts
void UExpirenceSystem::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


int UExpirenceSystem::GainExp(int value)
{
	this->currentExpirence += value;
	return value;
}

void UExpirenceSystem::LevelUp()
{
	if (this->currentExpirence >= nextExpirence)
	{
		this->currentLevel++;

	}
}

int UExpirenceSystem::CalculateLeftExp()
{
	int remainingValue = this->currentExpirence - exprienceList[currentLevel + 1];
	return remainingValue;
}

int UExpirenceSystem::GetCurLevel()
{
	return this->currentLevel;
}

int UExpirenceSystem::ExpRelease(int KillingSpree)
{
	//first calculate current exp total;
	int expirencecalculation = baseExp + 0.14 * currentExpirence;
	if (KillingSpree >= 10)
	{
		KillingSpree = 10;
	}
	expirencecalculation = expirencecalculation + KillingSpreeBounty[KillingSpree + 1];

	return expirencecalculation;
}

// Called every frame
void UExpirenceSystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

