// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseAttributes.h"

// Sets default values for this component's properties
UBaseAttributes::UBaseAttributes()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}



int UBaseAttributes::GetStrength()
{
	return this->str;
}

int UBaseAttributes::GetAgility()
{
	return this->agi;
}

int UBaseAttributes::GetIntelligence()
{
	return this->intl;
}

void UBaseAttributes::SetStrength(int strValue){
	this->str = str;
}

void UBaseAttributes::SetAgility(int agiValue)
{
	this->agi = agi;
}

void UBaseAttributes::SetIntelligence(int intlValue)
{
	this->intl = intl;
}
