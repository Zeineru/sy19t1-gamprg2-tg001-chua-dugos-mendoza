// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthPoints.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOTA2CLONE_API UHealthPoints : public UActorComponent
{
	GENERATED_BODY()

	public:
	// Sets default values for this actor's properties
	UHealthPoints();

	protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	int32 baseHP = 200;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	float maxHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attributes")
	float currentHP;

	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	public:

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	void SetInitialHealth(float strength);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float Heal(float value);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	float Damage(float value);

	UFUNCTION(BlueprintCallable, Category = "Attributes")
	void SetHealth(float value);

	public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;	
};