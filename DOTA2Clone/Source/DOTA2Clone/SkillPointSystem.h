// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SkillPointSystem.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOTA2CLONE_API USkillPointSystem : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USkillPointSystem();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SkillPointSystem")
	int32 unusedSkillPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SkillPointSystem")
	int32 totalSkillPoint;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


public:	
	// Called every frame
	UFUNCTION(BlueprintCallable, Category = "SkillPointSystem")
	void GainSkillPoint();

	UFUNCTION(BlueprintCallable, Category = "SkillPointSystem")
	void spendSkillPoint();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
