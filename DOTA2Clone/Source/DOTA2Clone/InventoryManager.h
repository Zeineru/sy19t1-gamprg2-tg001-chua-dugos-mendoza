// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ItemObject.h"
#include "InventoryManager.generated.h"

UCLASS()
class DOTA2CLONE_API AInventoryManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInventoryManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Item")
		int GiveItem(AItemObject* item);

	UFUNCTION(BlueprintCallable, Category = "Item")
		int SellItem(AItemObject* item);
};
