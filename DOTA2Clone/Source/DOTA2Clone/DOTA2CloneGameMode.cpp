// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "DOTA2CloneGameMode.h"
#include "DOTA2ClonePlayerController.h"
#include "DOTA2CloneCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADOTA2CloneGameMode::ADOTA2CloneGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ADOTA2ClonePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}