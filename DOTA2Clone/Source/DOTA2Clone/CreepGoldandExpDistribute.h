// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GoldManager.h"
#include "ActorGold.h"
#include "CreepGoldandExpDistribute.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DOTA2CLONE_API UCreepGoldandExpDistribute : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCreepGoldandExpDistribute();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CreepGoldAndExp")
	int32 minGoldMelee;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CreepGoldAndExp")
	int32 maxGoldMelee;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CreepGoldAndExp")
	int32 minGoldRanged;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CreepGoldAndExp")
	int32 maxGoldRanged;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "CreepGoldAndExp")
	int32 ExpRelease;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "CreepGoldAndExp")
	void ReleaseGold();

	UFUNCTION(BlueprintCallable, Category = "CreepGoldAndExp")
	void ReleaseGoldRanged(UActorGold* wallet);

	UFUNCTION(BlueprintCallable, Category = "CreepGoldAndExp")
	void ReleaseGoldMelee(UActorGold* wallet);
};
